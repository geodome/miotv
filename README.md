# CoreDNS - MioTV Plugin

## Problem Statement

Mio TV is an IPTV Service provided by Singtel as part of the fibre broadband subscription package. The Mio TV service uses private domain names that are not registered on public DNS servers. These private domain names can only be resolved by the DNS server hosted on the Singtel Router. As a result, this impedes the use of DHCP server to specify 3rd party DNS servers, such as Google's 8.8.8.8.   

## Solution

Setup a conditional DNS forwarder as the primary name server in the DHCP server settings. If the DNS query originates from the TV box, then the DNS query is forwarded to the Singtel router. Otherwise, the DNS query is forwarded to 3rd party DNS server.

## Prerequisites

Install the latest version of the Golang compiler. Instructions available at https://www.golang.org/. 

## Compiling

First get the CoreDNS source code by running, after you cloned this repository into the proper path in your `GOPATH`
   
  
```bash
$ go get github.com/coredns/coredns
```

Then navigate to the coredns directory
   
  
```bash
$ cd $GOPATH/src/github.com/coredns/coredns
```

Next update the `plugin.cfg` in the root of the coredns repository as follows
   
```bash
$ echo "miotv:miotv" >> plugin.cfg
```

The plugins are chained in the order stated in `plugin.cfg`. Ideally, the `miotv:miotv` line should be inserted just before `forward:forward` but appending it at the end of `plugin.cfg` works too.

Next install the plugin in the plugin directory as follows
   
```bash
$ cd $GOPATH/src/github.com/coredns/coredns/plugin
$ git clone git@bitbucket.org:geodome/miotv.git
```

Finally build CoreDNS with the `miotv` plugin.

```bash
$ go generate && go build
```


## Configuring corefile

The `miotv` plugin should be used in conjunction with the `forward` plugin. The `forward` plugin is responsible for forwarding DNS queries to 3rd party DNS servers.  

The syntax for the  `miotv`plugin is `miotv <ip address of mio tv box> <ip address of the singtel router>`

### Example settings

For example:
```
. {
	miotv 192.168.1.100 192.168.1.1
	forward . tls://8.8.8.8
}
```

The setting above will redirect DNS queries from the Mio TV Box (address 192.168.1.100) to the DNS resolver on Singtel Router (address 192.168.1.1). Other queries will be forwarded to Google's 8.8.8.8 via TLS transport.  

## Configuring the DHCP Server

Set this conditional DNS Forwarder as the primary name server. Use DHCP reservation to assign a fixed IP to the Singtel Mio TV Box.

## Author

* Donaldson Tan

## License

This plugin is licensed under the MIT License. See [LICENSE](LICENSE) for more information.