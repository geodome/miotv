package miotv

import (
	"context"
	"github.com/coredns/coredns/plugin"
	"github.com/miekg/dns"
	"github.com/coredns/coredns/core/dnsserver"
	"github.com/caddyserver/caddy"
	"github.com/coredns/coredns/plugin/pkg/log"
	"errors"
	"net"
)

type MioTV struct {
	Next plugin.Handler
	Addr string
	Gateway string
	Client *dns.Client
}

const name string = "miotv"
var logger = log.NewWithPlugin(name)

func init() {
	plugin.Register("miotv", setup)
}

func setup(c *caddy.Controller) error {
	// the configuration syntax is: miotv <ip address of tv box> <ip address of gateway>
	// first parse the configuration
	a := MioTV{}
	parsed := false
	if c.Next() && c.Next() {
		a.Addr = c.Val()
		if c.Next() {
			a.Gateway = c.Val()
			a.Client = new(dns.Client)
			parsed = true
		}
	}
	// if configuration syntax is correct, then...
	if parsed {
		dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
			a.Next = next
			return a 
		})
		return nil
	}
	// incomplete configuration information, so...
	return errors.New("pls specify miotv <ip address of tv box> <ip address of gateway>")
}

func (m MioTV) Name() string {
	// This is required for the plugin.Handler interface
	return name
}

func (m MioTV) ServeDNS(ctx context.Context, w dns.ResponseWriter, r *dns.Msg) (int, error) {
	// This function is called when the plugin is triggered
	ip, _, _ := net.SplitHostPort(w.RemoteAddr().String())
	if ip == m.Addr {
		// if the DNS query originates from the TV box, redirect the DNS query to the Gateway.
		in, _, err := m.Client.Exchange(r, net.JoinHostPort(m.Gateway, "53"))
		if err == nil {
			logger.Info("Redirected MIO TV's DNS Query to ", m.Gateway)
			w.WriteMsg(in)
			return dns.RcodeSuccess, nil
		} else {
			logger.Info("Redirection of MIO TV's DNS Query failed")
			msg := new(dns.Msg)
			msg.SetRcode(r, dns.RcodeRefused)
			w.WriteMsg(msg)
			return dns.RcodeSuccess, nil
		}
	}
	// if the origin IP isn't the TV box, then pass to the next plugin for processing
	return plugin.NextOrFailure(name, m.Next, ctx, w, r)
}